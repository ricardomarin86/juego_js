document.addEventListener('keydown', function(evento){
	if(evento.keyCode == 32){
		console.log("salta");

		if(nivel.muerto == false){
			saltar();
		}else{
			nivel.velocidad = 9;
			nubes.velocidadnube = 2;
			nivel.muerto = false;
			nivel.marcador = 0;
			picoro.x = ancho + 100;
		}
	}

});


var imgPlayer, imgNube, imgCactus, imgSuelo;

function cargaImagenes(){

	imgPlayer = new Image();
	imgNube = new Image();
	imgPicoro = new Image();
	imgSuelo = new Image();

	imgPlayer.src = 'img/goku.png'
	imgNube.src = 'img/nubes.png'
	imgPicoro.src = 'img/picoro.png'
	imgSuelo.src = 'img/suelo.png'
}



var ancho = 950;
var alto = 400;

var canvas,ctx;

function inicializar(){
	canvas = document.getElementById('canvas');
	ctx = canvas.getContext('2d');
	cargaImagenes();
}


function borrarCanvas(){
	canvas.width = ancho;
	canvas.height = alto;
}

var suelo = 270;
var goku = {y: suelo, vy:0, gravedad:2, salto:30, vymax:9, saltando: false};
var picoro = {x: ancho + 100, y: suelo};
var nivel = {velocidad: 9, marcador: 0, muerto: false};
var nubes = {x: 400, y:100, velocidadnube: 2};
var suelog = {x:0, y:suelo + 100};



function dibujaPlayer(){

	ctx.drawImage(imgPlayer,0,0,284,277,50,goku.y,80,100);
}

function dibujaPicoro(){
	ctx.drawImage(imgPicoro,0,0,80,120,picoro.x,picoro.y,80,100);
}

function dibujaNube(){
	ctx.drawImage(imgNube,0,0,160,67,nubes.x,nubes.y,160,67);
}

function movimientoPicoro(){
	if(picoro.x < -100){
		picoro.x = ancho + 100;
		nivel.marcador ++;
	}else{
		picoro.x -= nivel.velocidad;
	}
}

//---------------------------------------------------------------------------
function dibujaSuelo(){
	ctx.drawImage(imgSuelo,suelog.x,0,950,30,0,suelog.y,950,30);
}

function movimientoSuelo(){
	if(suelog.x > 950){
		suelog.x = 0;
	}else{
		suelog.x += nivel.velocidad;
	}
}

//----------------------------------------------------------------------------

function movimientoNube(){
	if(nubes.x < -100){
		nubes.x = ancho + 100;
	}else{
		nubes.x -= nubes.velocidadnube;
	}
}

function saltar(){
	goku.saltando = true;
	goku.vy = goku.salto;
}

//Funcion gravedad es para que cuando se aplique el salto a goku lo retorne de nuevo a su posicion original.
function gravedad(){
	if(goku.saltando == true){

		if(goku.y - goku.vy - goku.gravedad > suelo){
			goku.saltando = false;
			goku.vy = 0;
			goku.y = suelo;
		}else{
			goku.vy -= goku.gravedad;
			goku.y -= goku.vy;
		}
	}
}


function colision(){
	if(picoro.x >= 80 && picoro.x <= 110){
		if(goku.y >= suelo){
			nivel.muerto = true;
			nivel.velocidad = 0;
			nubes.velocidadnube = 0;
		}
	}
}


function puntuacion(){
	ctx.font = "30px impact";
	ctx.fillstyle = '#555555';
	ctx.fillText(`${nivel.marcador}`,900,50);
	if(nivel.marcador > 5){
		nivel.velocidad = 15;
	}


	if (nivel.muerto == true) {
		ctx.font = "60px impact";
		ctx.fillText(`GAME OVER`,350,200);

	}
}
//BUCLE PRINCIPAL

var FPS = 50;
setInterval(function(){
	principal();
},1000/FPS);

function principal(){
	borrarCanvas();
	gravedad();
	colision();
	movimientoPicoro();
	movimientoNube();
	movimientoSuelo();
	dibujaSuelo();
	dibujaNube();
	dibujaPicoro();
	dibujaPlayer();
	puntuacion();
}
